export const appMixin = {
    methods: {
        resetDataName(vm, data_name) {
            vm[data_name] = vm.$options.data.apply(vm)[data_name];
        }
    }
}