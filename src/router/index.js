import Vue from 'vue';
import VueRouter from 'vue-router';

import Index from '@/views/Index';

import Home from '@/views/Home';
import home from '@/components/Home/home';
import myprofile from '@/components/Home/myprofile';

import About from '@/views/About';
import about from '@/components/About/about';




Vue.use(VueRouter);

const routes = [
  {
    path: '',
    component: Index,
    children: [
      {
        path: '',
        component: Home,
        children: [
          {
            path: '',
            name: 'home',
            component: home,
          },
          {
            path: 'myprofile',
            name: 'myprofile',
            component: myprofile,
          },
        ],
      },
      {
        path: 'about',
        component: About,
        children: [
          {
            path: '',
            name: 'about',
            component: about,
          },
        ],
      },
    ],
  },
];

const router = new VueRouter({
  routes,
});

export default router;
